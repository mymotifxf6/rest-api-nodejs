
require('dotenv').config();
const express = require('express');
const app = express();
const userRouter = require('./api/users/user.router');

// Convert JSON object to JavaScript object
app.use(express.json());

app.use('/api/users', userRouter);

const port = process.env.APP_PORT || 3000;
app.listen(port, () => { console.log(`Listening to port ${port}...`) });