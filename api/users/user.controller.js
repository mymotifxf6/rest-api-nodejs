require('dotenv').config();
const { createUser, getUsers, getUser, updateUser, deleteUser, getUserByEmail } = require('./user.service');
const { genSaltSync, hashSync, compareSync } = require('bcrypt');
const { sign } = require('jsonwebtoken');

module.exports = {

    login: (req, res) => {
        const body = req.body;

        getUserByEmail(body.email, (err, results) => {
            if(err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Invalid email or password"
                });                
            }
            const result = compareSync(body.password, results.password);
            if(result) {
                result.password = undefined;    // Need not send the password is JSON web token
                const jsonToken = sign({ result: results }, process.env.TOKEN_KEY, {
                    expiresIn: '1h'
                });
                return res.json({
                    success: 1,
                    message: "Login Successful",
                    token: jsonToken
                });
            } else {
                return res.json({
                    success: 0,
                    data: "Invalid email or password"
                });              
            }
        });
    },

    createUser: (req, res) => {
        const body = req.body;
        const salt = genSaltSync(10);
        body.password = hashSync(body.password, salt);
        
        createUser(body, (err, results) => {
            if(err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: "Database connection error"
                });
            }
            return res.status(200).json({
                success: 1,
                data: results
            });
        });        
    },

    getUsers: (req, res) => {
        getUsers((err, results) => {
            if(err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },

    getUser: (req, res) => {
        const id = req.params.id;
        getUser(id, (err, results) => {
            if(err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "User not found"
                });
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },

    updateUser: (req, res) => {
        const body = req.body;        

        updateUser(body, (err, results) => {
            if(err) {
                console.log(err);
                return err.sqlMessage;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Failed to update user"
                });
            }
            return res.json({
                success: 1,
                message: "User data updated successfully"
            });
        });
    },

    deleteUser: (req, res) => {
        const id = req.params.id;
        deleteUser(id, (err, results) => {
            if(err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "User not found"
                });
            }
            return res.json({
                success: 1,
                data: "User removed successfully"
            });
        });
    },
}