const { query } = require("../../config/database");
const pool = require("../../config/database");

module.exports = {

    // Fetching user by email for authentication
    getUserByEmail: (email, callBack) => {
        pool.query(
            `SELECT * FROM user_registration WHERE email = ?`,
            [email],
            (error, results, fields) => {
                if(error) { callBack(error) }
                return callBack(null, results[0]);
            }
        )
    },

    // Creating a new User
    createUser: (data, callBack) => {
        pool.query(
            `INSERT INTO user_registration(first_name, last_name, gender, email, password, mobile)
                VALUES(?,?,?,?,?,?)`,
            [data.first_name, data.last_name, data.gender, data.email, data.password, data.mobile],
            (error, results, fields) => {
                if (error) { return callBack(error); }
                return callBack(null, results);
            }
        );
    },

    // Fetching all Users
    getUsers: callBack => {
        pool.query(
            `SELECT id, first_name, last_name, gender, email, mobile FROM user_registration`,
            [],
            (error, results, fields) => {
                if (error) { return callBack(error); }
                return callBack(null, results);
            }
        )
    },

    // Fetching User by ID
    getUser: (id, callBack) => {
        pool.query(
            `SELECT id, first_name, last_name, gender, email, mobile FROM user_registration WHERE id = ?`,
            [id],
            (error, results, fields) => {
                if (error) { return callBack(error); }
                return callBack(null, results[0]);
            }
        )
    },

    // Updating User
    updateUser: (data, callBack) => {
        pool.query(
            `UPDATE user_registration SET first_name = ?, last_name = ?, gender = ?, email = ?, mobile = ? WHERE id = ?`,
            [data.first_name, data.last_name, data.gender, data.email, data.mobile, data.id],
            (error, results, fields) => {
                if(error) { return callBack(error); }
                return callBack(null, results);
            }
        )
    },

    // Remove User    
    deleteUser: (id, callBack) => {
        pool.query(
            `DELETE FROM user_registration WHERE id = ?`,
            [id],
            (error, results, fields) => {
                if (error) { return callBack(error); }
                return callBack(null, results);
            }
        )
    },
    
}